# Practice project 1 - Tour of Heroes

**Author**: Eddy Li

**Reviewer**: Sola 

**Due date:** 10/09/2021

**Introduction**: This *Tour of Heroes* tutorial shows you how to set up your local development environment and develop an application using the [Angular CLI tool](https://angular.io/cli), and provides an introduction to the fundamentals of Angular.

The *Tour of Heroes* application that you build helps a staffing agency manage its stable of heroes. The application has many of the features you'd expect to find in any data-driven application. The finished application acquires and displays a list of heroes, edits a selected hero's detail, and navigates among different views of heroic data.

## Quick start

```
1. clone the repo
2. cd angular-tour-of-heroes
3. ng serve --open
```

**Source**: https://angular.io/tutorial



